var s = chrome.storage.sync;
var sm = chrome.runtime.sendMessage;
var om = chrome.runtime.onMessage;

var resultTitleSelector = $("div.gs_r");
var autoSave;
var titleBar = $("<div/>", {
    "id": "titleBar",
    "class": "momane_tb",
    "style": "display:block;" +
        "background-color:gray;" +
        "height:28px;"
});

var scholarTitle = $("<div/>", {
    "id": "scholarTitle",
    "class": "momane_sTitle",
    "style": "display:block;" +
        "" +
        "font-weight:bolder;" +
        "padding-left:5px"
});

var editTa = $("<textarea/>", {
    "id": "editorTa",
    "class": "momane_et",
    "style": "display:block;" +
        "margin:5px 5px;" +
        "width:95%; " +
        "height: 90%;"
});
var editorDiv = $("<div/>", {
    "id": "editorFrame",
    "style": "display:none; " +
        "position:fixed; " +
        "z-index:9555586;" +
        "background-color: #fff;" +
        "box-shadow: 1px 1px 15px #888888;" +
        "right:0px;" +
        "top:0px;" +
        "width: 260px;" +
        "height:" + screen.height + "px;",
    "class": "momane_ed"

}).appendTo("body");

var closeIcon = $("<span/>", {
    "style": "position:inherit; " +
        "top:5px; " +
        "right:250px; " +
        "cursor:pointer;"
}).text("X").click(function (e) {
    saveNoteInOnce();
    setTimeout(function () {
        resetEditor(false);
    }, 200);

});
closeIcon.appendTo(titleBar);
titleBar.appendTo(editorDiv);
scholarTitle.appendTo(editorDiv);
editTa.appendTo(editorDiv);


//setInterval(function () {
changeSearchList();
//}, 2 * 500);

//change the search result page
function changeSearchList() {
    resultTitleSelector.each(function () {
        var sr = $(this);
        if (sr.find(".noteLink").length === 0) {
            addNoteLink(sr);
        }
    });
}

//add link to the result
function addNoteLink(selector) {
   
    s.get("notes", function (i) {
        
        var notes = i.notes;
        var linkSpan = $("<span/>", {
            "class": "noteLink",
            "style": "font-size: 13px; " +
                "font-weight: bolder;" +
                "cursor: pointer;" 
                
        });
        var hasNote = false;
        var title = selector.find("h3>a").text().trim().toLowerCase();
        var url = selector.find('.gs_md_wp.gs_ttss a').attr('href');
        //console.log(url)
//        var link = selector.find("h3>a").attr("href");
        if (notes) {
            for (var j = 0; j < notes.length; j++) {
                if (notes[j]) {
//                    console.log(title);
//                    console.log(notes[j].title + ".....");
//                    console.log(title == notes[j].title);
    

                    
                    console.log(url, notes[j].url)

                    if (url == notes[j].url || encodeURIComponent(url) == notes[j].url) {
                        hasNote = true;
                        console.log(121321)
//                        add check note link
                        var cn = $("<label/>", {
                            "class": "checkNote",
                            "style": "font-size:100%; cursor: pointer;",
                            "ref": j
                        }).text("Check Note").click(function (e) {
                            checkNote(j, $(this).closest(".gs_r"), e);
                        });
                        $('<span class="checkImg"></span>').appendTo(linkSpan);
                        cn.appendTo(linkSpan);
                        break;
                    }
                }
            }
        }

        if (!hasNote) {
            var an = $("<label/>", {
                "class": "addNote",
                "style": "font-size:100%; cursor: pointer;"
            }).text("Add Note").click(function (e) {
                var scholarLi = $(this).closest(".gs_r");
                addNote(scholarLi, e);
            });
            $('<span class="addImg"></span>').appendTo(linkSpan);
            an.appendTo(linkSpan);
        }
        selector.find("div[class='gs_fl']").append(linkSpan);
    });
}

function getNoteUrlFromLi(li){
    return li.find('.gs_md_wp.gs_ttss a').attr('href');
}

function getNoteTitleFromLi(li){
    return li.find('h3.gs_rt a').text();
}


//add new note
function addNote(scholarLi, e) {
    if (editTa.val() != "") {
        saveNoteInOnce();
        setTimeout(function () {
            resetEditor(true, scholarLi, e);
        }, 200);

    } else {
        resetEditor(true, scholarLi, e);
    }
}

function showEditorNote() {
    var title = "New note";
    s.get("notes", function(i) {

        var notes = i.notes;

        if (notes) {
            for (var j = 0; j < notes.length; j++) {
                if (notes[j]) {
                    if (url == notes[j].url || encodeURIComponent(url) == notes[j].url) {
                        var noteId = j;
                        title = notes[j].title;
                    }
                }
            }
        }
    });
    var url = window.location.href;
    s.get("notes", function (i) {
        var notes = i.notes;
        if (noteId&&editTa.val() != "") {
            saveNoteInOnce();
            setTimeout(function () {
                editTa.attr("ref", noteId);
                editTa.val(notes[noteId].content);
                scholarTitle.text(title);
                showEditor(e);
            }, 200);
        } else {
            //editTa.attr("ref", noteId);
            //editTa.val(notes[noteId].content);
            scholarTitle.text(title);
            showEditor();
        }
    });
}
//check and edit  note

function checkNote(noteId, scholarLi, e) {



    var title = getNoteTitleFromLi(scholarLi);
    var url = getNoteUrlFromLi(scholarLi);
    s.get("notes", function (i) {
        var notes = i.notes;
        if (editTa.val() != "") {
            saveNoteInOnce();
            setTimeout(function () {
                editTa.attr("ref", noteId);
                editTa.val(notes[noteId].content);
                scholarTitle.text(notes[noteId].title);
                showEditor(e);
            }, 200);
        } else {
            editTa.attr("ref", noteId);
            editTa.val(notes[noteId].content);
            scholarTitle.text(title);
            showEditor(e);
        }
    });
}


//function removeNote(noteId, e) {
//    if (editTa.val() != null) {
//        saveNoteWhenClose();
//    }
//    var c = confirm("Do you really want to remove this note? \n Can NOT UNDO");
//    if (c) {
//        delete noteRecord[noteId];
//        s.set({notes: noteRecord}, function () {
//
//        });
//    }
//}


//reset the editor before show a new one
function resetEditor(show, li, e) {

    var title = '';
    var url = '';

    if (li){
        title = getNoteTitleFromLi(li);
        url = getNoteUrlFromLi(li);
    }

    editTa.attr("data-title", title);
    editTa.attr("data-url", url);

    scholarTitle.text(title);
    editTa.removeAttr("ref");
    editTa.val("");
    if (show) {
        showEditor(e);
    } else {
        editorDiv.hide();
        clearInterval(autoSave);
    }

}


function showEditor() {
    if (!editorDiv.is(":visible")) {
        editorDiv.animate({ width: [ "toggle", "swing" ]}, 500);
    }
    editTa.focus();
    autoSaveNote();
}

// save notes to the database, in once, not automatically
function saveNoteInOnce() {
    s.get("notes", function (i) {
        var notes = i.notes;
        var content = editTa.val();
        var title = $.trim(editTa.attr("data-title").toLowerCase());
        var url = editTa.attr("data-url");

        if (content != "" && content) {
            var note = {title: title, content: content, url:url}
        }
        var noteID = editTa.attr("ref");

        if (typeof noteID != "undefined") {
            notes[noteID].content = content;
        } else {
            if (!notes) {
                notes = [];
            }
            notes.push(note);
        }

        notes.sort();
        s.set({notes: notes}, function () {
            changeEditNode(notes, note, title);
            localStorage.tempContent = "";
        });
    });
}

/*
s.set({notes: []}, function () {
          
        });
*/
//save automatically
function autoSaveNote() {
    autoSave = setInterval(function () {
        if (editorDiv.is(":visible")) {
            var curContent = editTa.val();
            var title = scholarTitle.text().toLowerCase();
            var noteId = editTa.attr("ref");
            var tempContent = localStorage.tempContent;
            if (curContent != "") {
                if (tempContent != curContent) {
                    localStorage.tempContent = curContent;
                    var note = {title: title, content: curContent};
                    s.get("notes", function (i) {
                        var notes = i.notes;
                        if (noteId) {
                            notes[noteId]["content"] = curContent;
                        } else {
                            if (!notes) {
                                notes = [];
                            }
                            notes.push(note);
                        }
                        s.set({"notes": notes}, function () {
                            changeEditNode(notes, note, title);
                        });
                    });
                }
            }
        }
    }, 10 * 1000);

}


//change add note to check Note
function changeEditNode(notes, note, title) {
    var j = notes.indexOf(note);
    resultTitleSelector.each(function () {
        var $this = $(this);
        if ($(this).find("h3>a").text() == title && $(this).find(".checkNote").length == 0) {
            $(this).find(".noteLink").remove();
            var linkSpan = $("<span/>", {
                "class": "noteLink",
                "style": "font-size: 13px; " +
                    "font-weight: bolder;" +
                    "cursor: pointer;" +
                    "text-decoration: underline;" +
                    "color:red"
            });
            var cn = $("<label/>", {
                "class": "checkNote",
                "style": "font-size:100%; cursor: pointer;",
                "ref": j
            }).text("Check Note").click(function (e) {
                checkNote(j, $this, e);
            });
            cn.appendTo(linkSpan);
        }
    });
}

