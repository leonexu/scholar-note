/**
 * Created by Hank on 6/16/2014.
 */
var s = chrome.storage.sync;
var sm = chrome.runtime.sendMessage;
var om = chrome.runtime.onMessage;
var titleBar = $("<div/>", {
    "id": "titleBar",
    "class": "momane_tb",
    "style": "display:block;" +
        "background-color:gray;" +
        "height:28px;"
});

var scholarTitle = $("<div/>", {
    "id": "scholarTitle",
    "class": "momane_sTitle",
    "contenteditable": "true",
    "style": "display:block;" +
        "" +
        "font-weight:bolder;" +
        "padding-left:5px"
}).text("Add the title here").click(function () {
    scholarTitle.select();
});

var editTa = $("<textarea/>", {
    "id": "editorTa",
    "class": "momane_et",
    "style": "display:block;" +
        "margin:5px 5px;" +
        "width:95%; " +
        "height: 90%;"
});
var editorDiv = $("<div/>", {
    "id": "editorFrame",
    "style": "display:none; " +
        "position:fixed; " +
        "z-index:9555586;" +
        "background-color: #fff;" +
        "box-shadow: 1px 1px 15px #888888;" +
        "right:0px;" +
        "top:0px;" +
        "width: 260px;" +
        "height:" + screen.height + "px;",
    "class": "momane_ed"

}).appendTo("body");

var closeIcon = $("<span/>", {
    "style": "position:inherit; " +
        "top:5px; " +
        "right:250px; " +
        "cursor:pointer;"
}).click(function (e) {
    saveNoteWhenClose();
});
titleBar.appendTo(editorDiv);
scholarTitle.appendTo(editorDiv);
editTa.appendTo(editorDiv);
closeIcon.appendTo(editorDiv);

// show edit panel
function showPanel() {
    try {
        if (!editorDiv.is(":visible")) {
            //scholarTitle.text(title);
            editorDiv.animate({ width: ["toggle", "swing"] }, 500);
        }
        editTa.focus();
        PDFJS.disableWorker = true;
        //debugger;
        PDFJS.workerSrc = 'chrome-extension://'+chrome.runtime.id+'/pdf.worker.js';
        //clearTitle('');
        //editorDiv.animate({ width: ["toggle", "swing"] }, 500);
        PDFJS.getDocument(window.location.href).then(function(data) {
            var e = data.getMetadata().then(function(data) {
                var lTitle = data.info.Title;
                loadNote(lTitle);
            });
        });
    } catch(e) {
        loadNote();
    }
}
function loadNote(title) {
    var url = window.location.href;
    var check = false;
    s.get("notes", function (i) {
        var pn = i.notes;
        if (pn) {
            for (var j = 0; j < pn.length; j++) {
                var note = pn[j];
                if (note && ((title != null && note.title == clearTitle(title)) || url == note.url || encodeURIComponent(url) == note.url)) {
                    editTa.attr("ref", j);
                    scholarTitle.text(note.title);
                    editTa.text(note.content);
                    check = true;
                }
            }
        }
    });
    if (!check) {
        scholarTitle.text(title);
    }
    if (!editorDiv.is(":visible")) {
        editorDiv.animate({ width: ["toggle", "swing"] }, 500);
    }
    editTa.focus();
    autoSaveNote();
}
//save note when close
function saveNoteWhenClose() {
    var title = scholarTitle.text();
    var link = window.location.href;
    var content = editTa.val();
    if (title && content) {
        title = clearTitle(title);
        s.get("notes", function (i) {
            var notes = i.notes;
            if (content != "") {
                var note = { title: title, content: content, url: link };
            }
            var noteID = editTa.attr("ref");
            if (typeof noteID != "undefined" && notes[noteID]!=null) {
                notes[noteID].content = content;
            } else {
                if (!notes) {
                    notes = [];
                }
                notes.push(note);
            }

            notes.sort();
            s.set({notes: notes}, function () {
                localStorage.tempContent = "";
                editorDiv.toggle("hide");
            });
        });
    }
}


//save automatically
function autoSaveNote() {
    autoSave = setInterval(function () {
        if (editorDiv.is(":visible")) {
            var curContent = editTa.val();
            var title = scholarTitle.text();
            if (title != "") {
                title = clearTitle(title);
            }
            var noteId = editTa.attr("ref");
            var tempContent = localStorage.tempContent;
            var link = window.location.href;
            if (curContent != "") {
                if (tempContent != curContent) {
                    localStorage.tempContent = curContent;
                    var note = {title: title, content: curContent, url: link};
                    s.get("notes", function (i) {
                        var notes = i.notes;
                        if (noteId) {
                            notes[noteId]["content"] = curContent;
                            notes[noteId]["title"] = title;
                        } else {
                            if (!notes) {
                                notes = [];
                            }
                            notes.push(note);
                        }
                        s.set({"notes": notes}, function () {
                            if (!noteId) {
                                editTa.attr("ref", notes.indexOf(note));
                            }

                        });
                    });
                }
            }
        }
    }, 10 * 1000);

}

function clearTitle(title) {
    return title.replace(/[\s+\n+]/g, " ").trim().toLowerCase();

}
