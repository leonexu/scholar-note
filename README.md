# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

(This plugin is created two years ago and it relies on a PDF loading module. Now the PDF loading module is banned by chrome and hence this plugin does not work correctly any more.)

I read academic papers using chrome and hope to take notes for the papers inside chrome. 

Many note plugins have existed in chrome. They can make a note for a certain URL but not for a certain paper, which causes problems: 

*   Multiple pdf copies exist for each paper on the web. 
*   Suppose that I hope to read paper ABC. I search ABC in Google and get u1 - a url to a certain copy of ABC. I create a note for u1. 
*   One month later I hope to read ABC again. This time Google returns u2 - url of another copy of ABC. My note made for u1 will not be shown on u2.

This chrome plugin automatically synchronizes notes made on different copies of the same paper. 

You see a "add note" button for each paper in google scholar when you install this plugin:
![1.png](https://bitbucket.org/repo/yzRqe8/images/4225880611-1.png)

You can add note to each paper:
![2.png](https://bitbucket.org/repo/yzRqe8/images/3932372330-2.png)

### How do I get set up? ###

* Clone this code. 
* Open the "developer mode" in chrome's 'setting -> extension'.
* Load the cloned code. 